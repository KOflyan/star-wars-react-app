export class Person {
    birth_year: string = '';
    edited: string = '';
    eye_color: string = '';
    gender: string = '';
    hair_color: string = '';
    height: string = '';
    mass: string = '';
    name: string = '';
    skin_color: string = '';
}


export interface ResultPropsI {
    people : Person[];
    searchText : string;
    offset : number;
}
