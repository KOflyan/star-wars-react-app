import * as React from "react";
import Header from "./layout/header";
import Footer from "./layout/footer";
import Search from "./search/search";

const App = () => (

    <div style={{marginTop: '10%'}}>
        <Header/>
        <Search/>
        <Footer/>
    </div>
)


export default App;