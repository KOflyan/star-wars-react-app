import * as React from 'react';
import axios from 'axios';
import Table from './table';
import { trackPromise, usePromiseTracker } from 'react-promise-tracker';
import LoadingIndicator from '../utils/loading';
import ReactPaginate from 'react-paginate';


const Search = () => {

    const [people, setPeople] = React.useState([]);
    const [search, setSearch] = React.useState('');
    const [offset, setOffset] = React.useState(0);

    const { promiseInProgress } = usePromiseTracker();

    const fetch = async () => {

        let response = await axios.get(`https://swapi.co/api/people/?search=${search}`);

        if (!response || !response.data || !response.data.results || !search || search.trim().length === 0) {
            setPeople([]);
            return;
        };
        
        setPeople(response.data.results);            
    };

    React.useEffect( () => {trackPromise(fetch())}, [search])    
    
    return (
        <div className="container">
                <div className="form-group">
                    <input type="search" className="form-control" id="search" placeholder="Luke Skywalker ..." onChange={(e) => setSearch(e.target.value)}/>
                </div>
            <div className="row">
                { 
                    promiseInProgress ? <LoadingIndicator/> : <Table people={people.slice(offset, offset + 5)} searchText={search} offset={offset}/> 
                }
            </div>
            {
                !promiseInProgress && search && people.length > 0 &&
                    <div className="row mt-2">
                        <div className="col text-center">
                            <ReactPaginate
                                pageCount={Math.ceil(people.length / 5)}
                                pageRangeDisplayed={5}
                                marginPagesDisplayed={2}
                                onPageChange={(item) => setOffset(Math.ceil(item.selected * 5))}
                                breakClassName="page-item"
                                breakLabel={<a className="page-link">...</a>}
                                activeClassName="active"
                                pageClassName="page-item"
                                previousClassName="page-item"
                                nextClassName="page-item"
                                pageLinkClassName="page-link btn"
                                previousLinkClassName="page-link"
                                nextLinkClassName="page-link"
                                containerClassName={'pagination justify-content-center'}
                            />
                        </div>
                    </div>
            }
        </div>
    )
}

export default Search;

