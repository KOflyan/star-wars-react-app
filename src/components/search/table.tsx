import * as React from 'react';
import { ResultPropsI, Person } from '../../model/models';

const Table = (props : ResultPropsI) => {

    const people = props.people, text = props.searchText, offset = props.offset;
    const keys = Object.keys(new Person()) as (keyof Person)[];


    if (people.length === 0) {
        return (
            <div className="col text-center mt-3">{
                (text && text.length > 0) ? 'Nothing found for your query :(' : 'Start typing something!'
            }</div>
        )
    }

    return (
        <div className="col mx-0 mt-3 text-center">
            <table className="table table-striped table-dark">
                <thead>
                    <tr>
                        <td>#</td>
                        {keys.map( (k, i) => <th key={i}>{k}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {
                        props.people.map( (person, i) => 
                            (
                                <tr key={i}>
                                    <td>{i + offset + 1}</td>
                                    {keys.map( (k, j) => <td key={j}>{person[k]}</td>)}
                                </tr>
                            )
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default Table;