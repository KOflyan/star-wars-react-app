import React = require("react");

const Header = () => (
    <div className="container mb-5 pb-4">
        <div className="row">
            <div className="col text-center">
                <h1>Star Wars Legends Searcher</h1>
            </div>
        </div>
    </div>
)


export default Header;