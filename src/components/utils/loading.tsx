import * as React from 'react';
import Loader from 'react-loader-spinner';

const LoadingIndicator = () => ( 
    <div className="loader">
        <Loader type="ThreeDots" color="white" height={100} width={100} />
    </div>
);


export default LoadingIndicator;