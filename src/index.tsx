
import App from './components/app';
import * as React from "react";
import * as ReactDOM from "react-dom";
import './css/main.css';

ReactDOM.render(
    <App/>,
    document.getElementById('app')
);