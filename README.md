Start Wars Legends Searcher
===========================

Live demo: http://15.188.3.43/rakuten/

Project tree
------------
```
star-wars-react-app
├── public
└── src
    ├── components
    │   ├── layout
    │   ├── search
    │   └── utils
    ├── css
    ├── model
    └── types
```

Overview
--------

This is a simple React + Webpack app, which allows user to search for a Star Wars legend
dynamically. This app is just front end, which requests data from external API - https://swapi.co.



Work Progress
-------------

Basically not much to explain here, Ive used webpack for running/building the project
as it is most commonly used tool for that scenario. For data fetching Ive used axios, since
Ive worked with it before and it is quite handy. Ive also used following libs: `react-paginate`, 
`react-spinner`, `react-promise-tracker`.
I have separated logic as much as I could/felt necessary, although
there is always space for various optimizations.



Deploy
------

1) Clone this repo

```bash
git clone git@gitlab.com:KOflyan/star-wars-react-app.git
cd star-wars-react-app
```

2) Install dependencies

```bash
npm i
```

3.1) Dev mode:

```bash
npm start
```

3.2) Production mode:

```bash
npm run build
```

Open `dist/index.html` in your browser.